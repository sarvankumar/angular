import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-to-do-component',
  templateUrl: './create-to-do-component.component.html',
  styleUrls: ['./create-to-do-component.component.css']
})
export class CreateToDoComponentComponent implements OnInit {
  title:string="Create to do list";
  toDoListData:any[] = [];
  doneToDoListData:any[] = [];
  constructor() { }

  ngOnInit(): void {
   
  }


  onSubmit(data, taskForm:NgForm){
    if(data.taskName == '' && data.taskDescription == ''){
      taskForm.resetForm();
    }else if(data.taskName != null && data.taskDescription != null){
      this.toDoListData.push(data);
      taskForm.resetForm();
    }
    else{
      taskForm.resetForm();
      console.log("all fields are required");
    }
  }

}
