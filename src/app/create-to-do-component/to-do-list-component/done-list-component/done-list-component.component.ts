import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-done-list-component',
  templateUrl: './done-list-component.component.html',
  styleUrls: ['./done-list-component.component.css']
})
export class DoneListComponentComponent implements OnInit {
  @Input() doneList:any;
  @Output() passData = new EventEmitter();

  uncheckedList:any[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  checkedDoneData(index){
    setTimeout (() => {
      this.uncheckedList.push(this.doneList[index]);
      this.passData.emit(this.uncheckedList);
      this.doneList.splice(index,1);
   }, 1000);

  }

}
