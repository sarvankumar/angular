import { Component, OnInit, Input, ViewChild } from '@angular/core';
//import { DoneListComponentComponent } from './done-list-component/done-list-component.component';

@Component({
  selector: 'app-to-do-list-component',
  templateUrl: './to-do-list-component.component.html',
  styleUrls: ['./to-do-list-component.component.css']
})
export class ToDoListComponentComponent implements OnInit {
  //@ViewChild(DoneListComponentComponent) toDoDoneList:any;
  @Input() toDoList:any=[];
  doneList:any[] = [];
  constructor() { }

  ngOnInit(): void {
    console.log(this.toDoList);
  }

  checkedData(index){
    setTimeout (() => {
      this.doneList.push(this.toDoList[index]);
      this.toDoList.splice(index,1);

      console.log(this.doneList);
   }, 1000);

  }

  receiveData($event){
    console.log($event[0]);
    this.toDoList.push($event[0]);
    console.log(this.toDoList);
  }

  // ngAfterViewInit() {
  //   this.toDoList = this.toDoDoneList.uncheckedList;
  // }

}
